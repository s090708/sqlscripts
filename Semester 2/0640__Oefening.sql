USE aptunes;
DROP PROCEDURE IF EXISTS GetLiedjes;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetLiedjes` (IN nameSong VARCHAR(50))
BEGIN
    SELECT *
    FROM Liedjes
    WHERE Titel LIKE CONCAT('%', nameSong, '%');
END $$

DELIMITER ;