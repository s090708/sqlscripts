USE ApTunes; 
DROP PROCEDURE IF EXISTS GetAlbumDuration;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration`(IN album_id INT, OUT totalLength SMALLINT)
BEGIN

DECLARE OK INTEGER DEFAULT 0;
    DECLARE songLength TINYINT UNSIGNED;

    DECLARE currentSong
        CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = album_id;

DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET OK = 1;

    OPEN currentSong;
        SET totalLength = 0;
			getLength: LOOP
		FETCH currentSong INTO songLength;
            IF OK = 1
                THEN
                LEAVE getLength;
            END IF;
		SET totalLength = totalLength + songLength;
    END LOOP;

CLOSE currentSong;
END$$

DELIMITER ;