USE ModernWays;

INSERT INTO Personen (
   Voornaam, 
   Familienaam
)
VALUES (
'Jean-Paul', 
'Sartre');

INSERT INTO Boeken (
    Titel,
    Verschijningsdatum,
    Uitgeverij,
    Personen_Id
)
VALUES (
   'De woorden',
   '1961',
    'de Bezige Bij',
   (SELECT Id FROM Personen WHERE
       Familienaam = 'Sartre' AND Voornaam = 'Jean-Paul'))