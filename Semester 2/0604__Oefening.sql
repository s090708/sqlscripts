USE ModernWays;
CREATE TABLE Uitleningen(
Personen_Id INT,
CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY(Personen_Id)
REFERENCES Leden(Id),
Boeken_Id INT,
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY(Boeken_Id)
REFERENCES Boeken(Id),
Startdatum DATE not null,
Einddatum DATE);
