USE Aptunes;
DROP PROCEDURE IF EXISTS DemonstrateHandlerOrder;

DELIMITER $$
USE `Aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN

     DECLARE randomGetal INT DEFAULT FLOOR(RAND()*(3-1+1)+1);

    DECLARE EXIT HANDLER FOR SQLSTATE '45002'
        BEGIN
            SELECT 'State 45002 opgevangen. Geen probleem';
        END;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
        BEGIN
            SELECT 'Algemene fout opgevangen';
        END;

    IF randomGetal = 1 THEN
        SIGNAL SQLSTATE '45001';
    ELSEIF randomGetal = 2 THEN
        SIGNAL SQLSTATE '45002';
    ELSE
        SIGNAL SQLSTATE '45003';
    END IF;

END $$

DELIMITER ;