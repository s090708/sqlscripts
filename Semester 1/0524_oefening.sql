USE Modernways;

SET SQL_SAFE_UPDATES = 0;
DELETE FROM Huisdieren 
WHERE (Baasje = 'Thaïs' AND Soort = 'hond') 
OR (Baasje = 'Truus' AND Soort = 'kat');
SET SQL_SAFE_UPDATES = 1;