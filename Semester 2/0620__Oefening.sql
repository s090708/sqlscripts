USE Aptunes;

CREATE INDEX VoornaamFamilienaamIdx
ON Muzikanten(Voornaam, Familienaam);