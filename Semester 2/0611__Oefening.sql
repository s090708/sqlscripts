USE ModernWays;

SELECT Games.Titel, IF(Platformen.Id IS NOT NULL, Platformen.Naam, 'Geen platformen gekend') AS Naam
FROM Games
LEFT JOIN Releases ON Games.Id = Releases.Games_Id
LEFT JOIN Platformen ON Platformen.Id = Releases.Platformen_Id
WHERE Platformen.Id IS NULL

UNION ALL
SELECT IF(Games.Id IS NOT NULL,Games.Titel, 'Geen games gekend'), Platformen.Naam
FROM Games
RIGHT JOIN Releases ON Games.Id = Releases.Games_Id
RIGHT JOIN Platformen ON Platformen.Id = Releases.Platformen_Id
WHERE Games.Id IS NULL