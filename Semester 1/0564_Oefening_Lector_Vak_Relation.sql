USE Hogeschool;
CREATE TABLE Geeft(
Lector_Personeelsnummer INT NOT NULL,
Vak_Naam VARCHAR(100),
CONSTRAINT fk_Geeft_Lector 
FOREIGN KEY (Lector_Personeelsnummer) 
REFERENCES Lector(Personeelsnummer),
CONSTRAINT fk_Geeft_Vak
FOREIGN KEY (Vak_Naam)
REFERENCES Vak(Naam)
);
					