USE aptunes;
DROP PROCEDURE IF EXISTS MockAlbumReleasesLoop;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN

DECLARE counter INT DEFAULT 0;

mockingLoop: LOOP
CALL MockAlbumReleaseWithSuccess(@succes);
    IF @succes = 1 THEN
		SET counter = counter + 1;
    END IF;
    IF counter = extraReleases THEN
        LEAVE mockingLoop;
    END IF;
END LOOP;
END $$

DELIMITER ;