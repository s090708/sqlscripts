USE aptunes;
DROP PROCEDURE IF EXISTS DangerousInsertAlbumReleases;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumReleases`()
BEGIN
-- declareren variabelen
    DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    DECLARE counter INT DEFAULT 1;

-- handlers declareren
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
        BEGIN
            ROLLBACK;
        END;

-- albums en band aantal ophalen
    SELECT COUNT(*)
    INTO numberOfAlbums
    FROM Albums;
    SELECT COUNT(*)
    INTO numberOfBands
    FROM Bands;

-- random getal kiezen 
    SELECT FLOOR(RAND() * (numberOfAlbums - 1 + 1) + 1)
    INTO randomAlbumId;
    SELECT FLOOR(RAND() * (numberOfBands - 1 + 1) + 1)
    INTO randomBandId;

-- poging tot random combinatie 
    REPEAT
        START TRANSACTION;
            INSERT INTO Albumreleases(bands_id, albums_id)
                VALUE (randomBandId, randomAlbumId);
            IF counter = 2 AND FLOOR(RAND() * (3 - 1 + 1) + 1) THEN
                SIGNAL SQLSTATE '45000';
            END IF;
        COMMIT;
        SET counter = counter + 1;
        UNTIL counter = 3
        END
        REPEAT;

END
$$

DELIMITER ;