USE ModernWays; 
ALTER VIEW AuteursBoeken AS
SELECT CONCAT(Voornaam, ' ', Familienaam) AS 'Auteur',  Boeken.Titel AS 'Titel', Boeken.Id AS 'Id'
FROM Personen 
 
INNER JOIN Boeken ON Personen.Id = Boeken.Id; 