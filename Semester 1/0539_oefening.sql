USE ModernWays;
SELECT AVG(Leeftijd) AS 'Gemiddelde Leeftijd', Max(Leeftijd) AS 'Hoogste leeftijd', COUNT(*) AS 'Totaal aantal dieren'
FROM Huisdieren;
