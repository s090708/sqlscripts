-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
USE ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

-- primary key toevoegen en meedere gegevens aan de Personen tabel
-- AanspreekTitel, Adress(straat, Hnr, stad), Commentaar en Biografie
ALTER TABLE Personen ADD (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null
);

-- connectie maken tussen boeken en personen
-- foreign key & gegevens toevoegen aan boeken

ALTER TABLE Boeken ADD Personen_Id INT NULL;
UPDATE Boeken CROSS JOIN Personen
SET Boeken.Personen_Id = Personen.Id
WHERE Boeken.Voornaam = Personen.Voornaam 
AND	Boeken.Familienaam = Personen.Familienaam;
ALTER TABLE Boeken CHANGE Personen_Id Personen_Id INT NOT NULL;

-- kolommen die dubbel voorkomen uit de tabel boeken verwijderen
ALTER TABLE Boeken DROP COLUMN Voornaam, DROP COLUMN Familienaam;

-- foreign key aanvullen met constraint
ALTER TABLE Boeken ADD CONSTRAINT fk_Boeken_Perosonen
FOREIGN KEY(Personen_Id) REFERENCES Personen(Id);