USE aptunes;
DROP PROCEDURE IF EXISTS MockAlbumReleases;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleases` (IN extraReleases INT)
BEGIN

-- variabelen declareren
    DECLARE counter INT DEFAULT 0;

REPEAT
    CALL MockAlbumReleaseWithSuccess(@succes);
    IF @succes = 1 THEN
        SET counter = counter + 1;
    END IF;
UNTIL counter = extraReleases
END REPEAT;

END $$

DELIMITER ;