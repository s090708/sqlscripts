USE aptunes;
DROP PROCEDURE IF EXISTS DemonstrateHandlerOrder;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder`()
BEGIN


    DECLARE randomGetal INT DEFAULT FLOOR(RAND() * (3 - 1 + 1) + 1);

    DECLARE EXIT HANDLER FOR SQLSTATE '45002'
        BEGIN
            SELECT 'State 45002 opgevangen. Geen probleem';
        END;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
        BEGIN
            RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
        END;

    IF randomGetal = 1 THEN
        SIGNAL SQLSTATE '45001'
            SET MESSAGE_TEXT = 'Algemene fout opgevangen';
    ELSEIF randomGetal = 2 THEN
        SIGNAL SQLSTATE '45002';
    ELSE
        SIGNAL SQLSTATE '45003'
            SET MESSAGE_TEXT = 'Algemene fout opgevangen';
    END IF;

END $$

DELIMITER ;