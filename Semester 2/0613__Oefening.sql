USE ModernWays; 
CREATE VIEW AuteursBoeken 
AS SELECT CONCAT(Voornaam, ' ', Familienaam) AS 'Auteur',  Boeken.Titel AS 'Titel'
FROM Personen 
 
INNER JOIN Boeken ON Personen.Id = Boeken.Id; 
