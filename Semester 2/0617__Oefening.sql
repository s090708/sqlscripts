USE ModernWays;

CREATE VIEW AuteursBoekenRatings
AS
SELECT Auteur, Titel, Rating
FROM AuteursBoeken
INNER JOIN GemiddeldeRatings;