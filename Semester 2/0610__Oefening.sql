USE ModernWays;

SELECT Games.Titel, Platformen.Naam
FROM Games
RIGHT JOIN Releases on Games.Id = Releases.Games_Id
RIGHT JOIN Platformen on Platformen.Id = Releases.Platformen_Id