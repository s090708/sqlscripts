USE ModernWays;
SELECT Studenten_Id FROM Evaluaties
INNER JOIN Studenten
ON Evaluaties.Studenten_Id = Studenten.Id
GROUP BY Studenten_Id
HAVING AVG(Cijfer) > (select AVG(Cijfer) FROM Evaluaties); 