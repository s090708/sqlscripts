USE Modernways;

ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR (20) CHAR SET utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren SET Geluid = 'woef' WHERE Soort = 'Hond';
UPDATE Huisdieren SET Geluid = 'miauw' WHERE Soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;
