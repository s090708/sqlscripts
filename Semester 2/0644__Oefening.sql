USE aptunes;
DROP PROCEDURE IF EXISTS MockAlbumRelease;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease` ()
BEGIN

-- variabelen declareren
    DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    DECLARE combinationExist INT DEFAULT 1;

-- aantal albums en band selecteren
    SELECT COUNT(*)
    INTO numberOfAlbums
    FROM Albums;
    SELECT COUNT(*)
    INTO numberOfBands
    FROM Bands;

-- random getal kiezen tussen het aantal en 1
    SELECT FLOOR(RAND()*(numberOfAlbums-1+1)+1)
    INTO randomAlbumId;
    SELECT FLOOR(RAND()*(numberOfBands-1+1)+1)
    INTO randomBandId;

-- checken of de combinatie randomAlbumId en randomBandId al voorkomt
    SELECT EXISTS(
        SELECT *
        FROM Albumreleases
        WHERE Bands_Id = randomBandId && Albums_Id = randomAlbumId
        )
    INTO combinationExist;

-- als combinationExist gelijk is aan 0 kan de combinatie toegevoegd worden
    IF combinationExist = 0 THEN
        INSERT INTO Albumreleases(
            bands_id, albums_id
        ) VALUE (
            randomBandId,randomAlbumId
        );
    END IF;


END $$

DELIMITER ;